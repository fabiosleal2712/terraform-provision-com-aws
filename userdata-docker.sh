#!/bin/bash
# Install Docker
echo "Update the apt package index and install packages to allow apt to use a repository over HTTPS:"
  sudo apt-get update
  sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release -y

echo "Add Docker’s official GPG key:"
    sudo mkdir -p /etc/apt/keyrings
    sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
  
echo "Use the following command to set up the repository:"
    echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

echo "Update the apt package index, and install the latest version of Docker Engine, containerd, and Docker Compose, or go to the next step to install a specific version:"

    sudo apt-get update -y
    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y


echo "Install Kumbernetes"
  sudo apt update
  sudo swapoff -a; sed -i '/swap/d' /etc/fstab
  sudo cat >>/etc/sysctl.d/kubernetes.conf<<EOF
    net.bridge.bridge-nf-call-ip6tables = 1
    net.bridge.bridge-nf-call-iptables = 1
EOF
{
  sudo apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
  sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  sudo apt update
  sudo apt install -y docker-ce=5:19.03.10~3-0~ubuntu-focal containerd.io
}

{
  sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
  sudo echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
}

  sudo apt update && apt install -y kubeadm=1.18.5-00 kubelet=1.18.5-00 kubectl=1.18.5-00

{
  mknod /dev/kmsg c 1 11
  echo '#!/bin/sh -e' >> /etc/rc.local
  echo 'mknod /dev/kmsg c 1 11' >> /etc/rc.local
  chmod +x /etc/rc.local
}




#kubeadm join 172.31.6.9:6443 --token k3b7fj.3c16cjfusmnlipem \
    --discovery-token-ca-cert-hash sha256:052feebd9efe964bf27ef3b0a72d20ab25c7dfc06f7dbad16826f20ff6bf031c

# kubectl apply -f https://docs.projectcalico.org/v3.14/manifests/calico.yaml

#
#export KUBECONFIG=/etc/kubernetes/admin.conf