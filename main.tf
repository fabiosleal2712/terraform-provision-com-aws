provider "aws" {
  region = "us-east-1"
}


resource "aws_key_pair" "k8s-key" {
  key_name   = "k8s-key"
  public_key = file("./k8s-key.pub")
}

resource "aws_security_group" "k8s-sg" {
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "ssh"
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

}

resource "aws_instance" "kumbernetes-worker" {
  ami           = "ami-0ca614dcaddfdd29f"
  instance_type = "t2.micro"
  key_name      = "k8s-key"
  count         = 2
  user_data = file("./userdata-docker.sh")
  tags = {
    name = "k8s"
    type = "worker"
  }
  security_groups = ["${aws_security_group.k8s-sg.name}"]
}

resource "aws_instance" "kumbernetes-master" {
  ami           = "ami-0ca614dcaddfdd29f"
  instance_type = "t2.micro"
  key_name      = "k8s-key"
  count         = 1
  user_data = file("./userdata-docker.sh")
  tags = {
    name = "k8s"
    type = "master"
  }
  security_groups = ["${aws_security_group.k8s-sg.name}"]
}