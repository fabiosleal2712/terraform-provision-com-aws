terraform {
    backend "s3" {
        region = "us-east-1"
        bucket = "client-empre8489384"
        encrypt = "true"
        key = "terraform.tfstate"
    }
}